import React, { useState, useEffect } from "react";
import { Routes, Route, useNavigate } from "react-router-dom";
import GameBoard from "../GameComponents/GameBoard";
import EndPage from "../GameComponents/EndPage";
import StartPage from "../GameComponents/StartPage";

// This code generates a list of 6 random numbers which
// will formulate the categories for the game.
let urlNumsList = [];
while (urlNumsList.length < 6) {
    let urlNum = Math.floor(Math.random() * 28162 + 1);
    if (!urlNumsList.includes(urlNum)) {
        urlNumsList.push(urlNum);
    }
}

export default function Game() {
    const [categoryList, setCategoryList] = useState([]);
    const [selectedClue, setSelectedClue] = useState({});
    const [showClueBool, setShowClueBool] = useState(false);
    const [showAnswerBool, setShowAnswerBool] = useState(false);
    const [selectedCategory, setSelectedCategory] = useState({});
    const [selectedValue, setSelectedValue] = useState(0);
    const [score, setScore] = useState(0);
    const [fetchProgress, setFetchProgress] = useState(0);
    const [showPlayButton, setShowPlayButton] = useState("d-none");
    const [showLoadingBar, setShowLoadingBar] = useState("");
    const [showFetchError, setShowFetchError] = useState(false);

    let navigate = useNavigate();

    // This function loops over the 6 category id's and runs a fetch for each.
    // It then stores the data from the fetch under the declared variable tempCategoryList.
    // It also hides and shows the loading bar and start button depending on the
    // status of the 6 fetches.
    useEffect(() => {
        let tempCategoryList = [];
        const fetchCategories = async () => {
            for (let i = 0; i < urlNumsList.length; i++) {
                const response = await fetch(
                    `https://jservice.io/api/category?id=${urlNumsList[i]}`
                );
                if (response.ok) {
                    const data = await response.json();
                    tempCategoryList.push(data);
                    setFetchProgress(tempCategoryList.length);
                }

                // This block needs to be debugged because it isn't catching if we get a 429
                else if (response.error) {
                    console.log("Sorry you got a bad response!!!");
                    setShowFetchError(true);
                } else {
                    console.log("Sorry you got a bad response!!!");
                    setShowFetchError(true);
                }
                if (tempCategoryList.length < 6) {
                    setShowLoadingBar("");
                    setShowPlayButton("d-none");
                } else {
                    setShowLoadingBar("d-none");
                    setShowPlayButton("");
                }
                if (showFetchError) {
                    setShowLoadingBar("d-none");
                    setShowPlayButton("d-none");
                    setShowFetchError(true);
                }
            }
        };
        fetchCategories();
        setCategoryList(tempCategoryList);
    }, [showFetchError]);

    // This code selects the correct category list and clue from based on indexes.
    // Then turns on the modal which shows the clue.
    function selectClue(category, clueNumber) {
        setSelectedValue((clueNumber + 1) * 200);
        if (category === 0) {
            setSelectedClue(categoryList[0].clues[clueNumber]);
            setSelectedCategory(categoryList[0]);
        } else if (category === 1) {
            setSelectedClue(categoryList[1].clues[clueNumber]);
            setSelectedCategory(categoryList[1]);
        } else if (category === 2) {
            setSelectedClue(categoryList[2].clues[clueNumber]);
            setSelectedCategory(categoryList[2]);
        } else if (category === 3) {
            setSelectedClue(categoryList[3].clues[clueNumber]);
            setSelectedCategory(categoryList[3]);
        } else if (category === 4) {
            setSelectedClue(categoryList[4].clues[clueNumber]);
            setSelectedCategory(categoryList[4]);
        } else if (category === 5) {
            setSelectedClue(categoryList[5].clues[clueNumber]);
            setSelectedCategory(categoryList[5]);
        }
        setShowClueBool(true);
    }

    // Simple function which replaces the clue modal with the answer modal.
    function showAnswer() {
        setShowClueBool(false);
        setShowAnswerBool(true);
    }

    // Simple function which ends the turn, hiding the answer modal.
    function endTurn(gotAnswerCorrect) {
        console.log(gotAnswerCorrect);
        setShowAnswerBool(false);
    }

    // Simple function which navigates to the gameboard from the start page.
    function gameStart() {
        navigate("/gameboard", { replace: true });
    }

    return (
        <div>
            <Routes>
                <Route
                    exact
                    path="/"
                    element={
                        <StartPage
                            gameStart={gameStart}
                            fetchProgress={fetchProgress}
                            showPlayButton={showPlayButton}
                            showLoadingBar={showLoadingBar}
                            showFetchError={showFetchError}
                        />
                    }
                />
                <Route
                    exact
                    path="gameboard/"
                    element={
                        <GameBoard
                            category1={categoryList[0]}
                            category2={categoryList[1]}
                            category3={categoryList[2]}
                            category4={categoryList[3]}
                            category5={categoryList[4]}
                            category6={categoryList[5]}
                            selectClue={selectClue}
                            showAnswer={showAnswer}
                            showClueBool={showClueBool}
                            selectedClue={selectedClue}
                            showAnswerBool={showAnswerBool}
                            selectedCategory={selectedCategory}
                            selectedValue={selectedValue}
                            endTurn={endTurn}
                            score={score}
                            setScore={setScore}
                        />
                    }
                />
                <Route element={<EndPage />} />
            </Routes>
        </div>
    );
}
