# Treampardy

Treampardy is a really fun game co-developed by Cindy Chiang, Lindsey Carlson, and Jeff Pettit. It is modeled off of the infamous game show Jeopardy!™ and utilizes real questions from past episodes of the show.

[CLICK HERE to play Treampardy!](https://treamforce.gitlab.io/treampardy-take-2/)

---

#### What is Tream??
Tream was formed during the August - Pacific cohort of Hack Reactor's 19-week software engineering bootcamp. Jeff, Lindsey, Cindy, and Ally (another Tream member) had been frequently working together as a group when one day Jeff stumbled on while trying to say "Teamwork makes the dream work". The word Tream emerged and the group was never the same!

#### So Treampardy huh?
The group wanted to build a game and decided to model it off of Jeopardy!™. Of course the only appropriate name for this game would be *Treampardy*.

The game utilizes an open-source Jeopardy!™ questions database called *jService* (jservice.io, github.com/sottenad/jService). It is built entirely using [React](https://www.react.dev), and hosted via [GitLab Pages](https://docs.gitlab.com/ee/user/project/pages/). The project also uses components from [React-Bootstrap](react-bootstrap.github.io).

Please note that while the project is live, it is also a continuous work-in-progress.
