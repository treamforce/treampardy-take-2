import React from "react";
import Button from "react-bootstrap/Button";
import ProgressBar from "react-bootstrap/ProgressBar";
import Alert from "react-bootstrap/Alert";

export default function StartPage(props) {
    return (
        <>
            <div className="px-4 py-5 my-5 text-center">
                <img
                    alt=""
                    src={`${process.env.PUBLIC_URL}/Treampardy-Logo.svg`}
                    width="300"
                    height="300"
                    className="d-inline-block align-top"
                />{" "}
                <div className="col-lg-6 mx-auto">
                    <h1>This is Treampardy!</h1>
                    <br />
                    <div className="d-grid gap-2">
                        <ProgressBar
                            className={props.showLoadingBar}
                            animated
                            variant="success"
                            now={props.fetchProgress}
                            max={6}
                        />
                        <Button
                            className={`buttonStyles ${props.showPlayButton}`}
                            size="lg"
                            onClick={(e) => props.gameStart()}
                        >
                            Click to begin
                        </Button>
                        <Alert
                            show={props.showFetchError}
                            variant="danger">
                            <Alert.Heading>Well crap!</Alert.Heading>
                            <p className="mb-0">
                                There was an error communicating with the database containing the questions.
                                Try waiting a couple of minutes then <b>refresh</b> the page.
                            </p>
                        </Alert>
                    </div>
                </div>
                <br />
                <br />
                <br />
                <p>Cindy Chiang</p>
                <p>Lindsey Carlson</p>
                <p>Jeff Pettit</p>
                <p>Version: 0.1</p>
            </div>
        </>
    );
}
